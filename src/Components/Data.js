import React, {useState} from 'react'
import "./Data.css"
import Logo from "../assets/logo.PNG"
import Folder from "./Folder"
import Text from "../assets/text.png"
import Image from "../assets/image.png"
import Video from "../assets/Video.png"
import Close from "../assets/close.png"
import DataViewer from './DataViewer';
import classNames from 'classnames';


function Data({codes, setCodes, setLoggedIn, role, statusMessage, setStatusMessage, fetchContent, backendFolders, baseUrl, data}) {

    const [openedData, setOpenedData] = useState(false);
    const [activeData, setActiveData] = useState("");
    const [files, setFiles] = useState({});
    const [uploadFolder, setUploadFolder] = useState("");
    const [activeMenu, setActiveMenu] = useState("uploadFile");
    const [newFolderValue, setNewFolderValue] = useState("");
    const [deleteFolder, setDeleteFolder] = useState("");
    console.log(codes)

    const chooseMenuOptions = [
        {value: "uploadFile", name: "Datei(en) hochladen"}, 
        {value: "addFolder", name: "Ablage hinzufügen"}, 
        {value: "removeFolder", name: "Ablage löschen"}, 
        {value: "addUser", name: "Nutzer hinzufügen"}, 
        {value: "removeUser", name: "Nutzer löschen"}, 

    ]

    useState(()=>{
        fetchContent();
    }, [])

    const handleLogout = () => {
        localStorage.removeItem("session");
        setLoggedIn(false);
    }

    const handleAddUser = () => {
        fetch(baseUrl+"/addUser", {
            method: "GET",
        }).then(async res => {
            const codes = await res.json();
            console.log(codes);
            setCodes(codes);
        })
    }

    const handleFolderDelete = () => {
        setStatusMessage("");
        if(deleteFolder!==""){
            fetch(baseUrl+"/deleteFolder", {
                method: "POST",
                body: JSON.stringify({
                    foldername: deleteFolder
                }),
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(async response => {
                const result = await response.text();
                setStatusMessage(result);
                fetchContent();
                setDeleteFolder("");
            }).catch(err => {setStatusMessage(JSON.stringify(err))})
        }else {
            setStatusMessage("Du musst eine Ablage auswählen");
        }
    }

    const handleAddFolder = (foldername) => {
        if(foldername.length > 3) {
            console.log(foldername);
            setStatusMessage("");
            fetch(baseUrl+"/addFolder", {
                method: "POST",
                body: JSON.stringify({
                    foldername: foldername
                }),
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(async response => {
                const res = await response.text();
                setStatusMessage(res);
                fetchContent();
                setFiles({});
                setUploadFolder("");
            }).catch(err => console.log(err));
        }else {
            setStatusMessage("Ordnername zu kurz");
        }
    }

    const handleFileInput = (files)=> {
        if(backendFolders.length > 1){
            setStatusMessage("");
            setFiles(files);
        }else {
            setStatusMessage("Erstelle erst eine Ablage.");
        }
    }

    const handleUpload = (folder, files) => {
        if(folder !== ""){
        const formData = new FormData();
        formData.append("folder", folder)

        for(let i = 0; i<files.length; i++){
            formData.append("files", files[i]);
            formData.append("filenames", files[i].name)
        }
        fetch(baseUrl+"/upload", {
            method: "POST",
            body: formData,
        }).then(async (response) => {
            const res = await response.text(); 
            setStatusMessage(res);
            fetchContent();
            setFiles({});
            setUploadFolder("");
            document.querySelector("#file-input").value="";
        })
        .catch(err => setStatusMessage(JSON.stringify(err))); 
    }else{
        setStatusMessage("Ein Ordner muss ausgewählt sein");
    }      
    }

    const handleRemove = (index) => {
        const dt = new DataTransfer()
        const input = document.getElementById('file-input')
        const { files } = input
        
        for (let i = 0; i < files.length; i++) {
          const file = files[i]
          if (index !== i)
            dt.items.add(file) 
        }
        input.files = dt.files
        setFiles(input.files)
    }
    console.log(role)

  return (
    <div className="data">
        <button className='logout-button' onClick={()=>handleLogout()}>Logout</button>
        <img src={Logo} className="logo" alt="logo"></img>
        {role=== "admin" ? 
        <div className='flex-center'>
            <h1 className="headline">Uploadmenü</h1>
            <div className="choose-menu">
                {chooseMenuOptions.map((option, idx)=>{
                    return <button key={idx} className={classNames({
                        "choose-menu-button": true,
                        "choose-menu-button--active": activeMenu === option.value
                    })} onClick={()=>{setActiveMenu(option.value); setStatusMessage("")}}>{option.name}</button>
                })}
            </div>
            {/* UPLOAD MENÜ */}
            <div className={classNames({"upload-menu-item": true, "active-menu": activeMenu === "uploadFile"})}>
                <input type="file" className="file-input" id="file-input" onInput={(e)=>handleFileInput(e.target.files, e.target.value)} multiple></input>
                <label htmlFor="file-input" className="file-input-label">
                    <img src={Text} className="file-image" alt="file"></img>
                    <br></br>
                    Datei auswählen
                </label>
                {/* ORDNER HINZUFÜGEN */}
                <div className={classNames({
                    "uploaded-files-container": true,
                    "display-none": Object.keys(files).length === 0
                })}>
                {Array.from(files).map((file, idx)=>{
                    return(
                        <div className="uploaded-file-container" key={idx}>
                            {file.type.includes("video") ? <img src={Video} alt="video" className="file-image file-image-small"></img>: ""}
                            {file.type.includes("image")? <img src={Image} alt="video" className="file-image file-image-small"></img>: ""}
                            {file.type.includes("application") || file.type.includes("text") ? <img src={Text} alt="video" className="file-image file-image-small"></img>: ""}
                            <span className="file-name">{file.name}</span>
                            <button className="delete-uploaded-file" onClick={()=>handleRemove(idx)}>
                                <img className="delete-icon" src={Close} alt="close"></img>
                            </button>
                        </div>
                    )
                })}
                <select className="folder-dropdown" name="folders" onChange={(e)=>setUploadFolder(e.target.value)} value={uploadFolder}>
                    <option value="">Wähle eine Ablage</option>
                    {backendFolders.map((folder, idx)=>{
                        if(folder!=="data"){ return <option key={idx} value={folder}>{folder}</option>}else{return null}
                    })}
                </select>
                <button className='upload-button' onClick={(e)=>handleUpload(uploadFolder, files)}>Upload</button>
            </div>
            </div>
            <div className={classNames({"upload-menu-item": true, "active-menu": activeMenu === "addFolder"})}>
                    <input className="text-input text-input--add-folder" value={newFolderValue} type="text" placeholder='Ordnernamen eingeben' onChange={(e)=>setNewFolderValue(e.target.value)}></input>
                    <button className='upload-button' onClick={(e)=> handleAddFolder(newFolderValue)}>Ordner erstellen</button>
            </div>
            <div className={classNames({"upload-menu-item": true, "active-menu": activeMenu === "removeFolder"})}>
                <select className="folder-dropdown" name="folders" onChange={(e)=>{setDeleteFolder(e.target.value); console.log(deleteFolder)}}  value={deleteFolder}>
                    <option value="">Wähle eine Ablage</option>
                    {backendFolders.map((folder, idx)=>{
                        if(folder!=="data"){ return <option key={idx} value={folder}>{folder}</option>}else{return null}
                    })}
                </select>
                <button className='upload-button' onClick={()=>handleFolderDelete(deleteFolder)}>Ablage löschen</button>
            </div>
            <div className={classNames({"upload-menu-item": true, "active-menu": activeMenu === "addUser"})}>
                <button className='upload-button' onClick={()=>handleAddUser()}>Nutzercode generieren</button>
                <div className="user-codes-container">
                    <p className="text-center">Ungenutzte Nutzercodes <br></br><span className="small-text">(Antippen um zu kopieren)</span></p>
                    {codes.map((code)=>{
                        return(
                            <button className="user-code-button" onClick={()=>navigator.clipboard.writeText(code).then(setStatusMessage("Code erfolgreich kopiert")).catch(err => setStatusMessage("Code konnte nicht kopiert werden: " + err))}>{code}</button>
                        )
                    })}
                </div>
            </div>
        </div>
        : null}
        <div onClick={()=>setStatusMessage("")} className={classNames({
            'status-message': true,
            "active-status-message": statusMessage && statusMessage !== ""
            })}>{statusMessage ? <span>{statusMessage} <br></br><span className="close-hint">(Antippen um zu schließen)</span></span> : "Schließen..."}</div>
        <h1 className="headline">Geteilte Ablage</h1>
        <div className="data-outter-container">
            {Object.keys(data).map((folder, idx)=>{
                if(folder!== "data"){
                    return(
                        <>
                            <Folder name={folder} folder={data[folder]} key={idx} setActiveData={setActiveData} setOpenedData={setOpenedData}></Folder>
                        </>
                    )
                }else {
                    return null;
                }
            })}
            <DataViewer setStatusMessage={setStatusMessage} fetchContent={fetchContent} baseUrl={baseUrl} activeData={activeData} openedData={openedData} setOpenedData={setOpenedData} data={data}></DataViewer>
        </div>
    </div>
  )
}

export default Data