import React from 'react'
import Image from "../assets/image.png"
import Video from "../assets/Video.png"
import Text from "../assets/text.png"
import "./File.css"

function File({file, setActiveData, setOpenedData}) {

    const videoFormats = ["mp4", "avi", "wmv", "mov", "mkv", "flv", "webm", "mpeg"];
    const imageFormats = ["jpg", "jpeg", "png", "gif", "bmp", "svg", "webp", "tiff"];
    const dataFormats = ["txt", "pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "zip", "rar"];


  const handleOnClick = () => {
    setOpenedData(true);
    setActiveData(file);
  }

    return(
        <div className="file-container" onClick={()=>handleOnClick()}>
            <div className='file-image-container'>
                {videoFormats.includes(file.filetype) ? <img src={Video} alt="video" className="file-image"></img>: ""}
                {imageFormats.includes(file.filetype) ? <img src={Image} alt="video" className="file-image"></img>: ""}
                {dataFormats.includes(file.filetype) ? <img src={Text} alt="video" className="file-image"></img>: ""}
            </div>
            <div className="info-container not-clickable">
                <div className="info-container-inner">
                    <div className="info-inner-left">
                        <p className="info-p">Dateiname: <br></br><span className="custom-color">{file.filename ? file.filename : "unavailable"}</span></p>
                        <p className="info-p">Dateiart: <br></br><span className="custom-color">{file.filetype ? file.filetype : "unavailable"}</span></p>
                    </div>
                    <div className="info-inner-right">
                        {(file.size/100000).toFixed(2)}MB
                    </div>
                </div>
            </div>
        </div>
    )

}

export default File