import React, {useState} from 'react'
import classNames from 'classnames';
import "./Folder.css"
import Arrow from "../assets/arrow.png"
import File from "./File.js"

function Folder({name, folder, setActiveData, setOpenedData}) {

  const [isOpened, setIsOpened] = useState(false);

  return (
    <div>
        <div className="files-container-outter">
          <div className="flex-space-between custom-hover" onClick={()=>setIsOpened(!isOpened)}>
              <h4 className="project-headline">{name}</h4>
              <button className={classNames({
                "folder-toggle-button": true,
                "open-folder": isOpened === false
              })}>
                <img src={Arrow} className="arrow-img" alt="arrimage"></img>
              </button>
          </div>
          <div className={classNames({
            "files-container-inner": true,
            "display-none": isOpened === false
          })}>
              {folder.map((file, idx)=>{
                return <File file={file} key={idx} setActiveData={setActiveData} setOpenedData={setOpenedData}></File>
              })}
          </div>
      </div>
    </div>
  )
}

export default Folder