import React, {useState} from 'react'
import "./Login.css"
import Logo from "../assets/logo.PNG"
import sha256 from "js-sha256";

function Login({setRole, statusMessage, setStatusMessage, baseUrl, setLoggedIn}) {

    const [activeView, setActiveView] = useState("login");
    const [userName, setUserName] = useState("");
    const [password,setPassword] = useState("");


    const handleLoginRequest = (username, password) => {
        fetch(baseUrl+"/login", {
            method: "POST",
            body: JSON.stringify({
                username: userName,
                password: password
            }),
            headers: {
                "Content-Type": "application/json"
            },
            credentials: "include"
        }).then(async res => {
            const response = await res.json();
            if(response.isLoggedIn === true){
                setLoggedIn(response.isLoggedIn);
                setStatusMessage(response.message);
                setRole(response.role);
                const cookies = res.headers.get("Set-Cookie");
                localStorage.setItem("session", JSON.stringify({IsLoggedIn: response.isLoggedIn, Role: response.role, Username: response.username}))
            }else {
                setStatusMessage(response.message)
            }
        })
    }

  return (
      <div class="login flex-center-column">
        <img src={Logo} alt="logo"className="margin-bottom-1"></img>
        {activeView === "login" && (
            <div className="form">
                <input className="custom-login-input" placeholder="Nutzername" onChange={(e)=>setUserName(e.target.value)} value={userName}></input>
                <input className="custom-login-input" type="password" placeholder="Passwort" onChange={(e)=>{setPassword(sha256(e.target.value))}}></input>
                <button className="submit" onClick={()=>handleLoginRequest(userName, password)}>Anmelden</button>
                <button className="register" onClick={()=>{setActiveView("register")}}>Registrieren</button>
            </div>
        )}
        {activeView ==="register" && (
            <div className="form">
                <input class="custom-login-input" placeholder="Zugangscode"></input>
                <input class="custom-login-input" placeholder="E-Mail"></input>
                <input class="custom-login-input" type="password" placeholder="Passwort"></input>
                <button class="submit">Registrieren</button>
                <button className="register" onClick={()=>{setActiveView("login")}}>Anmelden</button>
            </div>
        )}
        {statusMessage !== "" ? <p className='login-status'>{statusMessage}</p> : null}
    </div>
  )
}

export default Login