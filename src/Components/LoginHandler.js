import React, { useState } from "react"
import Data from "./Data";
import Login from "./Login";

function LoginHandler({codes, setCodes, role, setRole, loggedIn, setLoggedIn, fetchContent, backendFolders, baseUrl, data}) {

  const [statusMessage, setStatusMessage] = useState("");

  return (
    <div className="app-container">
        {loggedIn === false ? <Login role={role} setRole={setRole} statusMessage={statusMessage} setStatusMessage={setStatusMessage} setLoggedIn={setLoggedIn} baseUrl={baseUrl}></Login> : <Data codes={codes} setCodes={setCodes} setLoggedIn={setLoggedIn} fetchContent={fetchContent} role={role} statusMessage={statusMessage} setStatusMessage={setStatusMessage} backendFolders={backendFolders} baseUrl={baseUrl} data={data}></Data>}
    </div>
  )
}

export default LoginHandler