import classNames from 'classnames'
import React from 'react'
import "./DataViewer.css"
import Close from "../assets/close.png"
import Image from "../assets/image.png"
import Video from "../assets/Video.png"
import Text from "../assets/text.png"
import axios from "axios";
import Dump from "../assets/Papierkorb.png"
import FileDownload from "js-file-download";

function DataViewer({setStatusMessage, fetchContent, baseUrl, activeData, openedData, setOpenedData, data}) {

    const videoFormats = ["mp4", "avi", "wmv", "mov", "mkv", "flv", "webm", "mpeg"];
    const imageFormats = ["jpg", "jpeg", "png", "gif", "bmp", "svg", "webp", "tiff"];
    const dataFormats = ["txt", "pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "zip", "rar"];

    const checkFormat = (format) => {
        if(videoFormats.includes(format)) return "video";
        if(imageFormats.includes(format)) return "image";
        if(dataFormats.includes(format)) return "text";
    }
    const handleDeleteFile = (file, folder) => {
        if(file!=="Ordnerinformationen.txt") {
            const path = folder+"/"+file;
            fetch(baseUrl + "/deleteFile", {
                method: "POST",
                body: JSON.stringify({
                    path: path,
                }),
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(async response => {
                const res = await response.text();
                fetchContent();
                setOpenedData(false);
                setStatusMessage(res)
            }).catch(err=>{console.log(err)})
        }else {
            setStatusMessage("Ordnerinformationen können nicht gelöscht werden");
        }
    }
    const download = (e)=> {
        e.preventDefault();
        axios.get(
                baseUrl+"/download", {
                params: {
                    reqUrl: activeData.directory,
                    format: checkFormat(activeData.filetype)
                }, 
                responseType: "blob"
            }
        )
        .then((res)=> {
            FileDownload(res.data, activeData.filename)
        })
    }

    return (
    <div className={classNames({
        "data-viewer": true,
        "active-viewer": openedData
    })}>
        <button className="close-viewer" onClick={()=>setOpenedData(false)}>
            <img className="close-icon" src={Close} alt="close"></img>
        </button>
        {videoFormats.includes(activeData.filetype) ? <img src={Video} alt="video" className="file-image"></img>: ""}
            {imageFormats.includes(activeData.filetype) ? <img src={Image} alt="video" className="file-image"></img>: ""}
            {dataFormats.includes(activeData.filetype) ? <img src={Text} alt="video" className="file-image"></img>: ""}
        <table>
            <tr>
                <th>Dateiname:</th>
                <td>{activeData.filename}</td>
            </tr>
            <tr>
                <th>Dateigröße:</th>
                <td>{(activeData.size/100000).toFixed(2)}MB</td>
            </tr>
            <tr>
                <th>Dateiart:</th>
                <td>{activeData.filetype}</td>
            </tr>
            <tr>
                <th>Hochgeladen am:</th>
                <td>{activeData.uploaddate}</td>
            </tr>
            <tr>
                <th>Download:</th>
                <td className="helper-td">
                    <button onClick={(e)=>download(e)} className="download-button">{activeData.filename}</button>
                </td>
            </tr>
        </table>
        <button className="file-delete-button" onClick={()=>handleDeleteFile(activeData.filename, activeData.folder)}><img className="button-image-dump" src= {Dump} alt="Papierkorb"></img> Datei löschen</button>
    </div>
  )
}

export default DataViewer