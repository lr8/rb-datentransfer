import './App.css';
import React, { useState, useEffect } from "react";
import LoginHandler from './Components/LoginHandler';
import Logo from "./assets/logo.PNG"
const baseUrl = "http://localhost:5000"
const {IsLoggedIn, Role} = JSON.parse(localStorage.getItem("session")) || {IsLoggedIn: false, Role: "", Username: ""};

function App() {
  const [backendData, setBackendData] = useState({});
  const [backendFolders, setBackendFolders] = useState([]);
  const [loggedIn, setLoggedIn] = useState(IsLoggedIn || false)
  const [role, setRole] = useState(Role || "");
  const [codes, setCodes] = useState([]);


  const fetchContent =() => {
    fetch("/api").then(
      response => response.json()
    ).then(
      data=>{
        let preparedBackendData = {};
        let preparedObjectKeys = [];
        console.log(data);
        for(var k = 0; k<data.length; k++){
          if(!preparedObjectKeys.includes(data[k].folder)){
            preparedObjectKeys.push(data[k].folder);
            preparedBackendData[data[k].folder] = [];
          }
        }
        for(var e = 0; e<data.length; e++){
          preparedBackendData[data[e].folder].push(data[e]);
        }
        setBackendData(preparedBackendData);
        setBackendFolders(preparedObjectKeys);
      }
    ) 
    if(role === "admin") {
      fetch(baseUrl+"/getCodes").then(
        async response => {
          const fetchedCodes = await response.json();
          setCodes(fetchedCodes);
        }
      )
    }
  } 

  return (
    <div className="App">
        <LoginHandler codes={codes} setCodes={setCodes} fetchContent={fetchContent} role={role} setRole={setRole} loggedIn={loggedIn} setLoggedIn={setLoggedIn} backendFolders={backendFolders} baseUrl={baseUrl} data={backendData}></LoginHandler>
    </div>
  );
}

export default App;
