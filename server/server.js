const express = require ("express");
const app = express();
const cors = require("cors");
const fs = require("fs")
const path = require("path")
const baseDir = "data/";
const multer = require("multer");
const bodyParser = require("body-parser");
const users = require("./users.json");
const randomstring = require('randomstring');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "uploads/")
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})
const upload = multer({storage: storage})


app.use(cors({
    origin: "http://localhost:3000",
    credentials: true
}));
app.use(bodyParser.json());

function generateCode(){
    return randomstring.generate({
        length: 8,
        charset: "alphanumeric", 
        capitalization: "uppercase",
    })
}

app.get("/addUser", (req, res) => {
    try{
        let code;
        do{
            code = generateCode();
        }while(users.codes.includes(code))
        let newJSON = users;
        newJSON.codes.push(code);
        fs.writeFileSync("users.json", JSON.stringify(newJSON));
        res.send(users.codes);
    }catch(err) {
        console.log("Code erstellung fehlgeschlagen:" + err)
    }
})
app.get("/getCodes", (req, res) => {
    try{
        res.send(users.codes);
    }catch(err) {
        res.send(err);
    }
})

app.get("/api", (req, res) => {
    let result = [];
    try {
      const readDir = (dirPath) => {
        return new Promise((resolve, reject) => {
          fs.readdir(dirPath, (err, files) => {
            if (err) {
              reject(err);
            } else {
              resolve(files);
            }
          });
        });
      };
  
      const statFile = (filePath) => {
        return new Promise((resolve, reject) => {
          fs.stat(filePath, (err, fileStat) => {
            if (err) {
              reject(err);
            } else {
              resolve(fileStat);
            }
          });
        });
      };
  
      const getFileInfo = async (dirPath) => {
        const files = await readDir(dirPath);
        for (const file of files) {
          const filePath = path.join(dirPath, file);
          const fileStat = await statFile(filePath);
          if (fileStat.isDirectory()) {
            await getFileInfo(filePath);
          } else {
            const fileInfo = {
              folder: path.basename(dirPath),
              filename: file,
              size: fileStat.size,
              directory: filePath.replace(/\\/g, "/"),
              filetype: path.extname(file).replace(/\./g, ""),
              uploaddate: new Date(fileStat.birthtime).toLocaleDateString("de-DE", {
                year: 'numeric', 
                month: '2-digit', 
                day: '2-digit',
                hour: 'numeric',
                minute: 'numeric'
              })
            };
            result.push(fileInfo);
          }
        }
      };
  
      getFileInfo(baseDir).then(() => {
        res.json(result);
      });
    } catch (err) {
      res.send("Error");
    }
  });

  app.post("/login", (req, res) =>{
    const {username,password} = req.body
    const user = users.users.find(
        (user) => user.username === username && user.password === password
    );
    const admin = users.admins.find(
        admin => admin.username === username && admin.password === password
    )
    if(user) {
        res.cookie("session", "admin", {httpOnly: true, maxAge: 3600000});
        res.send({isLoggedIn: true, role: "user", message: "Herzlich Willkommen "+ user.username, username: username})
    }
    else if(admin) {
        res.cookie("session", "user", {httpOnly: true, maxAge: 3600000});
        res.send({isLoggedIn: true, role: "admin", message: "Herzlich Willkommen "+ admin.username, username: username})
    }
    else {
        res.send({isLoggedIn: false, role: "", message: "Anmeldung fehlgeschlagen. Username oder Passwort falsch."})
    }
  })

app.get("/download", (req, res) =>{
    try{
        res.download("./"+ req.query.reqUrl)
    }catch(err){
        console.log(err);
    }
})

app.post("/addFolder", (req, res) => {
    console.log(req.body.foldername)
    try{
        fs.mkdir(baseDir+req.body.foldername, (err) => {
            if(err) res.send("Ordnererstellung fehlgeschlagen (backend):" + err)
            res.send("Ordner erfolgreich erstellt")
        })
        fs.writeFile(baseDir+req.body.foldername+"/Ordnerinformationen.txt", "Ordner erstellt am "+ new Date().toLocaleDateString() + " durch den Admin.", (err)=>{if(err)throw err})
    }catch(err){
        res.send("Ordnererstellung fehlgeschlagen: " + err)
    }
})
app.post("/deleteFile", (req, res) => {
    try {
        fs.unlink(baseDir + req.body.path, (err)=> {
            if(err) res.send("Löschen fehlgeschlagen: " + err)
            res.send("Datei erfolgreich gelöscht");
        })
    }catch(err){
        res.send("Löschen fehlgeschlagen:" + err);
    }
})

app.post("/deleteFolder", (req, res) => {
    console.log(baseDir + req.body.foldername)
    try{
        fs.rmSync(baseDir+req.body.foldername, { recursive: true, force: true });
        res.send("Ordner " + req.body.foldername + " wurde erfolgreich gelöscht");
    }catch(err){
        res.send("Ordnerlöschung fehlgeschlagen: " + err)
    }
})


app.post("/upload", upload.array("files"), function (req, res, next){
    try {
        //Eine Datei verschieben wenn nur eine Hochgeladen wurde (Dann nämlich String und kein Array)
        if(typeof(req.body.filenames) === "string") {
            fs.rename("uploads/"+req.body.filenames, "data/"+req.body.folder+"/"+req.body.filenames, ()=>{
            });
        // Wenn Array mit Strings (mehrere Dateien) dann mit for loop verschieben
        }else if(Array.isArray(req.body.filenames)){
            for(var filename in req.body.filenames) {
                fs.rename("uploads/"+req.body.filenames[filename], "data/"+req.body.folder+"/"+req.body.filenames[filename], ()=>{
                });
            }
        }
        res.send("Dateien erfolgreich hochgeladen");
    }catch(err){
        res.send("Upload fehlgeschlagen. Fehlercode: " + err)
    }
})

app.listen(5000, ()=>{console.log("Server started on port 5000")});
